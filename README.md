# SKI-ing Skeletons #

This document describes the contents and ideas in the "Skiing Skeletons" paper.

## Introduction ##

* The idea of using SKI combinators to represent the semantics of alg. skel.
* Draft paper

## Contact ##

* Email: David Castro<dc84@st-andrews.ac.uk>
