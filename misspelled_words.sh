#!/bin/sh

IGNORE=$IGNORE"--add-tex-command='usetikzlibrary p' "
IGNORE=$IGNORE"--add-tex-command='bibliographystyle p' "
IGNORE=$IGNORE"--add-tex-command='input p' "
IGNORE=$IGNORE"--add-tex-command='documentclass op' "
IGNORE=$IGNORE"--add-tex-command='color p' "
IGNORE=$IGNORE"--add-tex-command='conferenceinfo pp' "
IGNORE=$IGNORE"--add-tex-command='copyrightdata op' "
IGNORE=$IGNORE"--add-tex-command='copyrightdoi op' "
IGNORE=$IGNORE"--add-tex-command='copyrightyear op' "
IGNORE=$IGNORE"--add-tex-command='special p' "
IGNORE=$IGNORE"--add-tex-command='draw op' "
IGNORE=$IGNORE"--add-tex-command='begin ppo' "
IGNORE=$IGNORE"--add-tex-command='kw op' "

ASPELL="aspell -p $PWD/aspell.en.pws -t $IGNORE list"

SED="sed '/\\begin{tikzpicture}/,/\\end{tikzpicture}/d'"
SED=$SED" | sed 's/\\<Ap>//'"

ALLFILES=`find . -iname '*.tex'`
#F="sections/combinatory_logic.tex"

for F in $ALLFILES
do
  WORDS=`cat $F | eval $SED | eval $ASPELL | sort | uniq`
  if [ ! -z "$WORDS" ]
  then
    echo "FILE: $F"
    echo "---------------------"
    for W in $WORDS
    do
      sed 's/\\input{.*}//' $F |
      sed 's/\\ref{.*}//' |
      sed 's/\\label{.*}//' |
        detex | egrep -I --include '\*.tex' --color $W
    done
    echo "---------------------"
  fi
done
